# Install GitLab Runner and CI cache server (Minio: s3 compatible object storage) on OpenShift, and enable cache configuration

## Prerequisites

Helm(v2) tiller pod already installed on OpenShift

## Install GitLab Runner and Minio 

```bash
# Create project for GitLab Runner and Minio
$ oc new-project cicd-runner
Now using project "cicd-runner" on server "https://xxxxxxxxxxx:443".

You can add applications to this project with the 'new-app' command. For example, try:

    oc new-app centos/ruby-25-centos7~https://github.com/sclorg/ruby-ex.git

to build a new example application in Ruby.

# Allow "edit" of cicd-runner project to tiller serviceaccount on tiller project
$ oc policy add-role-to-user --rolebinding-name=tiller edit "system:serviceaccount:${TILLER_NAMESPACE}:tiller" -n cicd-runner
role "edit" added: "system:serviceaccount:tiller:tiller"

$ oc get rolebinding tiller
NAME      ROLE      USERS     GROUPS    SERVICE ACCOUNTS   SUBJECTS
tiller    /edit                         tiller/tiller

# Create secret that includes "accesskey" and "secretkey" for access Minio
$ oc create secret generic s3access --from-literal=accesskey=<your-difined-accesskey> --from-literal=secretkey=<your-difined-secretkey>
secret/s3access created

# Install Minio using values.yaml
$ helm install --namespace cicd-runner --version 5.0.12 --name minio stable/minio -f minio-values.yaml
NAME:   minio
LAST DEPLOYED: Wed Mar  4 00:16:44 2020
NAMESPACE: cicd-runner
STATUS: DEPLOYED

RESOURCES:
==> v1/ConfigMap
NAME   AGE
minio  5m18s

==> v1/Deployment
NAME   AGE
minio  5m18s

==> v1/Pod(related)
NAME                    AGE
minio-5c448847bb-8l7nz  5m18s

==> v1/Service
NAME   AGE
minio  5m18s

==> v1/ServiceAccount
NAME   AGE
minio  5m18s


NOTES:

Minio can be accessed via port 9000 on the following DNS name from within your cluster:
minio.cicd-runner.svc.cluster.local

To access Minio from localhost, run the below commands:

  1. export POD_NAME=$(kubectl get pods --namespace cicd-runner -l "release=minio" -o jsonpath="{.items[0].metadata.name}")

  2. kubectl port-forward $POD_NAME 9000 --namespace cicd-runner

Read more about port forwarding here: http://kubernetes.io/docs/user-guide/kubectl/kubectl_port-forward/

You can now access Minio server on http://localhost:9000. Follow the below steps to connect to Minio server with mc client:

  1. Download the Minio mc client - https://docs.minio.io/docs/minio-client-quickstart-guide

  2. mc config host add minio-local http://localhost:9000 AKIAIOSFODNN7EXAMPLE wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY S3v4

  3. mc ls minio-local

Alternately, you can use your browser or the Minio SDK to access the server - https://docs.minio.io/categories/17

# To access Minio's GUI console from outside the cluster, you need to expose the service via Route by doing the following:
$ oc expose service minio 
route.route.openshift.io/minio exposed

# Allow SCC "anyuid" for default serviceaccount
$ oc adm policy add-scc-to-user anyuid -z default -n cicd-runner
scc "anyuid" added to: ["system:serviceaccount:cicd-runner:default"]

# Allow "edit" of cicd-runner project to default serviceaccount
$ oc policy add-role-to-user --rolebinding-name=gitlab-runner edit -z default -n cicd-runner

# Add helm chart repository of GitLab
$ helm repo add gitlab https://charts.gitlab.io

$ helm search gitlab/gitlab-runner
NAME                    CHART VERSION   APP VERSION     DESCRIPTION  
gitlab/gitlab-runner    0.13.1          12.7.1          GitLab Runner

# Create secret that includes "runner-registration-token" for access GitLab Server
$ oc create secret generic gitlab-runner --from-literal=runner-registration-token=<runner-registration-token-from-your-server> --from-literal=runner-token=""

# Install gitlab-runner pod using values.yaml
$ helm install --namespace cicd-runner --version 0.14.0 --name gitlab-runner gitlab/gitlab-runner -f gitlab-runner-values.yaml
```

## To check access to Minio on OpenShift

```bash
$ oc run -it mc --image=minio/mc --restart=Never --command --rm -- /bin/sh
If you don't see a command prompt, try pressing enter.
/ # mc config host add minio http://minio:9000 <your-accesskey> <your-secretkey>
Added `minio` successfully.
/ # mc ls minio
[2020-03-04 00:12:46 UTC]      0B distributed-ci-cache/
/ # exit
pod "mc" deleted
```